package main

import (
	"flag"
	"fmt"
	"io"
	"log"
	"net"
	"os"
	"os/signal"
	"strconv"
	"sync"

	"gitlab.com/devmaster64/go-mw-broadcaster/proxy"
)

var bPackets chan []byte
var vpnIP *string
var bPort int
var phyIP *string
var serverIP *string
var gamePort int
var mutex *sync.Mutex
var activeConns []*net.TCPConn

func main() {
	log.Println("need for speed most wanted vpn helper v1")
	isServer := flag.Bool("server", false, "run in server mode")

	phyIP = flag.String("phy", "", "physical interface IP address")
	serverIP = flag.String("serverIp", "", "server IP address")
	gamePort = 9900
	vpnIP = flag.String("vpn", "", "tap/tun interface IP address")
	bPort = 9899

	flag.Parse()

	log.Println("physical interface ip:", *phyIP)
	log.Println("vpn interface ip:", *vpnIP)
	if *isServer {
		mutex = &sync.Mutex{}
		activeConns = make([]*net.TCPConn, 0)
		bPackets = make(chan []byte, 1)

		c := make(chan os.Signal, 1)
		signal.Notify(c, os.Interrupt)
		go func() {
			for {
				<-c
				closeTCPBridgeConnections()
			}
		}()

		startTCPBridge()
		startBroadcastListener()
		startBroadcastProxy()
		startGameProxy()

	} else {
		startClient()
	}
}

func startTCPBridge() {
	log.Println("starting tcp bridge")
	tcpAddr, err := net.ResolveTCPAddr("tcp", *vpnIP+":"+strconv.Itoa(bPort))
	if err != nil {
		log.Fatalln("Could not resolve ", *vpnIP+":"+strconv.Itoa(bPort), " because", err)
	}
	l, err := net.ListenTCP("tcp", tcpAddr)
	if err != nil {
		log.Fatalln("Could not start TCP bridge listener because", err)
	}
	go func() {
		for {
			conn, err := l.AcceptTCP()
			if err != nil {
				log.Println("Could not accept TCP bridge connection because", err)
			}
			addTCPBridgeConnection(conn)
		}
	}()
}

func closeTCPBridgeConnections() {
	mutex.Lock()
	for _, conn := range activeConns {
		conn.Close()
	}
	mutex.Unlock()
}

func addTCPBridgeConnection(conn *net.TCPConn) {
	log.Println("got connection", conn.RemoteAddr())
	mutex.Lock()
	activeConns = append(activeConns, conn)
	mutex.Unlock()
}

func startBroadcastProxy() {
	log.Println("starting broadcast proxy. udp/9999 -> tcp/9899")
	go func() {
		for {
			packet := <-bPackets
			mutex.Lock()
			for _, conn := range activeConns {
				conn.Write(packet)
			}
			mutex.Unlock()
		}
	}()
}

func startGameProxy() {
	p := proxy.NewProxy(fmt.Sprintf("%s:%d", *vpnIP, gamePort), fmt.Sprintf("%s:%d", *phyIP, gamePort))
	p.Start()
}

func startBroadcastListener() {
	log.Println("starting udp broadcast listener")
	bAddr, err := net.ResolveUDPAddr("udp", ":9999")
	if err != nil {
		log.Fatalln("Could not resolve 255.255.255.255:9999 because", err)
	}
	l, err := net.ListenUDP("udp", bAddr)
	if err != nil {
		log.Fatalln("Could not start UDP listener because", err)
	}
	buffer := make([]byte, 384)
	go func() {
		for {
			for {
				_, err := l.Read(buffer)
				if err != nil {
					if err != io.EOF {
						log.Fatalln("Could not read broadcast packet because", err)
					}
					break
				}
				bPackets <- buffer
			}
		}
	}()
}

func startClient() {
	log.Println("running in client mode")
	bAddr, err := net.ResolveUDPAddr("udp", "255.255.255.255:9999")
	if err != nil {
		log.Fatalln(err)
	}
	log.Println("starting up broadcast listener")
	uConn, err := net.DialUDP("udp4", nil, bAddr)
	if err != nil {
		log.Fatalln(err)
	}
	tAddr, err := net.ResolveTCPAddr("tcp", *serverIP+":9899")
	if err != nil {
		log.Fatalln(err)
	}
	log.Println("connecting to tcp bridge")
	tConn, err := net.DialTCP("tcp", nil, tAddr)
	if err != nil {
		log.Fatalln(err)
	}
	log.Println("starting broadcast proxy. tcp/9899 -> udp/9999")
	go func() {
		for {
			buffer := make([]byte, 384)
			_, err := tConn.Read(buffer)
			if err != nil {
				log.Println(err)
			}
			_, err = uConn.Write(buffer)
			if err != nil {
				log.Println(err)
			}
		}
	}()
	log.Println("connecting to tcp bridge")

	p := proxy.NewProxy(fmt.Sprintf(":%d", gamePort), fmt.Sprintf("%s:%d", *serverIP, gamePort))
	p.Start()

}

// Errors to be understood
/*
1) multiple-value conn.conn.Write() in single-value context
*/
